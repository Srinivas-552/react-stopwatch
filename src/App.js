
// import StopWatch from './components/StopWatch';
import './App.css';
import StopWatchHooks from './components/StopWatch/StopWatchHooks';

function App() {
  return (
    <div className="App">
      {/* <StopWatch /> */}
      <StopWatchHooks />
    </div>
  );
}

export default App;
