import React, {Component} from 'react'
import './style.css'

class StopWatch extends Component {
    constructor(props){
        super(props)
        this.state = {
            initialTime: 0
        }
    }

    onResetTimer = () => {
        clearInterval(this.intervalId)
        this.setState({ initialTime: 0})
    }

    onStopTimer = () => {
        clearInterval(this.intervalId)
    }

    updateTime = () => {
        this.setState(prevState => ({
            initialTime: prevState.initialTime + 1
        })) 
    }

    onStartTimer = () => {
        clearInterval(this.intervalId)
        this.intervalId = setInterval(this.updateTime, 10)
    }

    renderTimer = () => {
         const {initialTime} = this.state
         
        const seconds = Math.floor((initialTime / 100) % 60);
        const minutes = Math.floor((initialTime / 100 / 60) % 60);
        const milliSeconds = Math.floor( initialTime % 100)

        let milliSecond = (milliSeconds < 10) ? `0${milliSeconds}` : milliSeconds
        let second = (seconds < 10) ? `0${seconds}` : seconds
        let minute = (minutes < 10) ? `0${minutes}` : minutes
        

        return `${minute}:${second}:${milliSecond}`
    }

    
    render(){

        return(
            <div>
                <h1 className='heading'>Stop Watch</h1>
                <p className="time">{this.renderTimer()}</p>
                <div>
                    <button className="button" onClick={this.onStartTimer}>Start</button>
                    <button className="button" onClick={this.onStopTimer}>Stop</button>
                    <button className="button" onClick={this.onResetTimer}>Reset</button>
                </div>
            </div>
        )
    }
}

export default StopWatch;