import React, {useState, useEffect} from 'react'
import './style.css'

function StopWatchHooks() {
    
    const [time, setTime] = useState(0)
    const [isTimerOn, setTimerStart] = useState(false)
    // const [isTimerStop, setTimerStop] = useState(false)
    // const [isTimerReset, setTimerReset] = useState(false)

    const updateTime = () => {
        setTime(prevTime => prevTime + 1)
    }

    useEffect(() => {
        let intervalId = null;

        if(isTimerOn){
            intervalId = setInterval(updateTime, 10)
        }
        else if(!isTimerOn) {
            clearInterval(intervalId)
        }
        else {
            setTimerStart(false)
            clearInterval(intervalId)
            setTime(0)
        } 

        return () => {
            clearInterval(intervalId)
        }

    }, [time, isTimerOn])

    

    const renderTimer = () => {
        
       const seconds = Math.floor((time / 100) % 60);
       const minutes = Math.floor((time / 100 / 60) % 60);
       const milliSeconds = Math.floor( time % 100)

       let milliSecond = (milliSeconds < 10) ? `0${milliSeconds}` : milliSeconds
       let second = (seconds < 10) ? `0${seconds}` : seconds
       let minute = (minutes < 10) ? `0${minutes}` : minutes
       

       return `${minute}:${second}:${milliSecond}`
   }


  return (
    <div>
        <h1 className='heading'>Stop Watch</h1>
        <p className="time">{renderTimer()}</p>
        <div>
            <button className="button" onClick={() => setTimerStart(true)}>Start</button>
            <button className="button" onClick={() => setTimerStart(false)}>Stop</button>
            <button className="button" onClick={() => setTime(0)}>Reset</button>
        </div>
    </div>

  )
}

export default StopWatchHooks;